<?php namespace Tests\Repositories;

use App\Models\ProductAgent;
use App\Repositories\ProductAgentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ProductAgentRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ProductAgentRepository
     */
    protected $productAgentRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->productAgentRepo = \App::make(ProductAgentRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_product_agent()
    {
        $productAgent = factory(ProductAgent::class)->make()->toArray();

        $createdProductAgent = $this->productAgentRepo->create($productAgent);

        $createdProductAgent = $createdProductAgent->toArray();
        $this->assertArrayHasKey('id', $createdProductAgent);
        $this->assertNotNull($createdProductAgent['id'], 'Created ProductAgent must have id specified');
        $this->assertNotNull(ProductAgent::find($createdProductAgent['id']), 'ProductAgent with given id must be in DB');
        $this->assertModelData($productAgent, $createdProductAgent);
    }

    /**
     * @test read
     */
    public function test_read_product_agent()
    {
        $productAgent = factory(ProductAgent::class)->create();

        $dbProductAgent = $this->productAgentRepo->find($productAgent->id);

        $dbProductAgent = $dbProductAgent->toArray();
        $this->assertModelData($productAgent->toArray(), $dbProductAgent);
    }

    /**
     * @test update
     */
    public function test_update_product_agent()
    {
        $productAgent = factory(ProductAgent::class)->create();
        $fakeProductAgent = factory(ProductAgent::class)->make()->toArray();

        $updatedProductAgent = $this->productAgentRepo->update($fakeProductAgent, $productAgent->id);

        $this->assertModelData($fakeProductAgent, $updatedProductAgent->toArray());
        $dbProductAgent = $this->productAgentRepo->find($productAgent->id);
        $this->assertModelData($fakeProductAgent, $dbProductAgent->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_product_agent()
    {
        $productAgent = factory(ProductAgent::class)->create();

        $resp = $this->productAgentRepo->delete($productAgent->id);

        $this->assertTrue($resp);
        $this->assertNull(ProductAgent::find($productAgent->id), 'ProductAgent should not exist in DB');
    }
}
