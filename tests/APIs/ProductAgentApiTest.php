<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ProductAgent;

class ProductAgentApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_product_agent()
    {
        $productAgent = factory(ProductAgent::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/product_agents', $productAgent
        );

        $this->assertApiResponse($productAgent);
    }

    /**
     * @test
     */
    public function test_read_product_agent()
    {
        $productAgent = factory(ProductAgent::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/product_agents/'.$productAgent->id
        );

        $this->assertApiResponse($productAgent->toArray());
    }

    /**
     * @test
     */
    public function test_update_product_agent()
    {
        $productAgent = factory(ProductAgent::class)->create();
        $editedProductAgent = factory(ProductAgent::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/product_agents/'.$productAgent->id,
            $editedProductAgent
        );

        $this->assertApiResponse($editedProductAgent);
    }

    /**
     * @test
     */
    public function test_delete_product_agent()
    {
        $productAgent = factory(ProductAgent::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/product_agents/'.$productAgent->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/product_agents/'.$productAgent->id
        );

        $this->response->assertStatus(404);
    }
}
