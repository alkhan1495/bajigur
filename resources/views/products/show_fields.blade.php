<!-- Sku Field -->
<div class="form-group">
    {!! Form::label('SKU', 'Sku:') !!}
    <p>{{ $product->SKU }}</p>
</div>

<!-- Aliassku Field -->
<div class="form-group">
    {!! Form::label('aliasSKU', 'Aliassku:') !!}
    <p>{{ $product->aliasSKU }}</p>
</div>

<!-- Productname Field -->
<div class="form-group">
    {!! Form::label('productName', 'Productname:') !!}
    <p>{{ $product->productName }}</p>
</div>

<!-- Aliasname Field -->
<div class="form-group">
    {!! Form::label('aliasName', 'Aliasname:') !!}
    <p>{{ $product->aliasName }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $product->description }}</p>
</div>

<!-- Uid Field -->
<div class="form-group">
    {!! Form::label('uid', 'Uid:') !!}
    <p>{{ $product->uid }}</p>
</div>

<!-- Unitmeasurementsmall Field -->
<div class="form-group">
    {!! Form::label('unitMeasurementSmall', 'Unitmeasurementsmall:') !!}
    <p>{{ $product->unitMeasurementSmall }}</p>
</div>

<!-- Unitmeasurementmedium Field -->
<div class="form-group">
    {!! Form::label('unitMeasurementMedium', 'Unitmeasurementmedium:') !!}
    <p>{{ $product->unitMeasurementMedium }}</p>
</div>

<!-- Unitmeasurementbig Field -->
<div class="form-group">
    {!! Form::label('unitMeasurementBig', 'Unitmeasurementbig:') !!}
    <p>{{ $product->unitMeasurementBig }}</p>
</div>

<!-- Ukuran Field -->
<div class="form-group">
    {!! Form::label('ukuran', 'Ukuran:') !!}
    <p>{{ $product->ukuran }}</p>
</div>

<!-- Nettweight Field -->
<div class="form-group">
    {!! Form::label('nettWeight', 'Nettweight:') !!}
    <p>{{ $product->nettWeight }}</p>
</div>

<!-- Nettweightum Field -->
<div class="form-group">
    {!! Form::label('nettWeightUM', 'Nettweightum:') !!}
    <p>{{ $product->nettWeightUM }}</p>
</div>

<!-- Volume Field -->
<div class="form-group">
    {!! Form::label('volume', 'Volume:') !!}
    <p>{{ $product->volume }}</p>
</div>

<!-- Smallinmediumamount Field -->
<div class="form-group">
    {!! Form::label('smallInMediumAmount', 'Smallinmediumamount:') !!}
    <p>{{ $product->smallInMediumAmount }}</p>
</div>

<!-- Mediuminbigamount Field -->
<div class="form-group">
    {!! Form::label('mediumInBigAmount', 'Mediuminbigamount:') !!}
    <p>{{ $product->mediumInBigAmount }}</p>
</div>

<!-- Pricebig Field -->
<div class="form-group">
    {!! Form::label('priceBig', 'Pricebig:') !!}
    <p>{{ $product->priceBig }}</p>
</div>

<!-- Pricesmall Field -->
<div class="form-group">
    {!! Form::label('priceSmall', 'Pricesmall:') !!}
    <p>{{ $product->priceSmall }}</p>
</div>

<!-- Pricemedium Field -->
<div class="form-group">
    {!! Form::label('priceMedium', 'Pricemedium:') !!}
    <p>{{ $product->priceMedium }}</p>
</div>

<!-- Currency Field -->
<div class="form-group">
    {!! Form::label('currency', 'Currency:') !!}
    <p>{{ $product->currency }}</p>
</div>

<!-- Priority Field -->
<div class="form-group">
    {!! Form::label('priority', 'Priority:') !!}
    <p>{{ $product->priority }}</p>
</div>

<!-- Isactive Field -->
<div class="form-group">
    {!! Form::label('isActive', 'Isactive:') !!}
    <p>{{ $product->isActive }}</p>
</div>

<!-- Start Date Field -->
<div class="form-group">
    {!! Form::label('start_date', 'Start Date:') !!}
    <p>{{ $product->start_date }}</p>
</div>

<!-- End Date Field -->
<div class="form-group">
    {!! Form::label('end_date', 'End Date:') !!}
    <p>{{ $product->end_date }}</p>
</div>

<!-- Stock Field -->
<div class="form-group">
    {!! Form::label('stock', 'Stock:') !!}
    <p>{{ $product->stock }}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{{ $product->image }}</p>
</div>

<!-- Category Id Field -->
<div class="form-group">
    {!! Form::label('category_id', 'Category Id:') !!}
    <p>{{ $product->category_id }}</p>
</div>

<!-- Createdby Field -->
<div class="form-group">
    {!! Form::label('createdBy', 'Createdby:') !!}
    <p>{{ $product->createdBy }}</p>
</div>

<!-- Updatedby Field -->
<div class="form-group">
    {!! Form::label('updatedBy', 'Updatedby:') !!}
    <p>{{ $product->updatedBy }}</p>
</div>

