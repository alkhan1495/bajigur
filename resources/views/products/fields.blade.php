<!-- Sku Field -->
<div class="form-group col-sm-6">
    {!! Form::label('SKU', 'Sku:') !!}
    {!! Form::text('SKU', null, ['class' => 'form-control','maxlength' => 191,'maxlength' => 191,'maxlength' => 191]) !!}
</div>

<!-- Aliassku Field -->
<div class="form-group col-sm-6">
    {!! Form::label('aliasSKU', 'Aliassku:') !!}
    {!! Form::text('aliasSKU', null, ['class' => 'form-control','maxlength' => 191,'maxlength' => 191,'maxlength' => 191]) !!}
</div>

<!-- Productname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('productName', 'Productname:') !!}
    {!! Form::text('productName', null, ['class' => 'form-control','maxlength' => 191,'maxlength' => 191,'maxlength' => 191]) !!}
</div>

<!-- Aliasname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('aliasName', 'Aliasname:') !!}
    {!! Form::text('aliasName', null, ['class' => 'form-control','maxlength' => 191,'maxlength' => 191,'maxlength' => 191]) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control','maxlength' => 191,'maxlength' => 191,'maxlength' => 191]) !!}
</div>

<!-- Uid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('uid', 'Uid:') !!}
    {!! Form::text('uid', null, ['class' => 'form-control','maxlength' => 191,'maxlength' => 191,'maxlength' => 191]) !!}
</div>

<!-- Unitmeasurementsmall Field -->
<div class="form-group col-sm-6">
    {!! Form::label('unitMeasurementSmall', 'Unitmeasurementsmall:') !!}
    {!! Form::text('unitMeasurementSmall', null, ['class' => 'form-control','maxlength' => 191,'maxlength' => 191,'maxlength' => 191]) !!}
</div>

<!-- Unitmeasurementmedium Field -->
<div class="form-group col-sm-6">
    {!! Form::label('unitMeasurementMedium', 'Unitmeasurementmedium:') !!}
    {!! Form::text('unitMeasurementMedium', null, ['class' => 'form-control','maxlength' => 191,'maxlength' => 191,'maxlength' => 191]) !!}
</div>

<!-- Unitmeasurementbig Field -->
<div class="form-group col-sm-6">
    {!! Form::label('unitMeasurementBig', 'Unitmeasurementbig:') !!}
    {!! Form::text('unitMeasurementBig', null, ['class' => 'form-control','maxlength' => 191,'maxlength' => 191,'maxlength' => 191]) !!}
</div>

<!-- Ukuran Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ukuran', 'Ukuran:') !!}
    {!! Form::text('ukuran', null, ['class' => 'form-control','maxlength' => 191,'maxlength' => 191,'maxlength' => 191]) !!}
</div>

<!-- Nettweight Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nettWeight', 'Nettweight:') !!}
    {!! Form::number('nettWeight', null, ['class' => 'form-control']) !!}
</div>

<!-- Nettweightum Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nettWeightUM', 'Nettweightum:') !!}
    {!! Form::text('nettWeightUM', null, ['class' => 'form-control','maxlength' => 191,'maxlength' => 191,'maxlength' => 191]) !!}
</div>

<!-- Volume Field -->
<div class="form-group col-sm-6">
    {!! Form::label('volume', 'Volume:') !!}
    {!! Form::number('volume', null, ['class' => 'form-control']) !!}
</div>

<!-- Smallinmediumamount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('smallInMediumAmount', 'Smallinmediumamount:') !!}
    {!! Form::number('smallInMediumAmount', null, ['class' => 'form-control']) !!}
</div>

<!-- Mediuminbigamount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mediumInBigAmount', 'Mediuminbigamount:') !!}
    {!! Form::number('mediumInBigAmount', null, ['class' => 'form-control']) !!}
</div>

<!-- Pricebig Field -->
<div class="form-group col-sm-6">
    {!! Form::label('priceBig', 'Pricebig:') !!}
    {!! Form::number('priceBig', null, ['class' => 'form-control']) !!}
</div>

<!-- Pricesmall Field -->
<div class="form-group col-sm-6">
    {!! Form::label('priceSmall', 'Pricesmall:') !!}
    {!! Form::number('priceSmall', null, ['class' => 'form-control']) !!}
</div>

<!-- Pricemedium Field -->
<div class="form-group col-sm-6">
    {!! Form::label('priceMedium', 'Pricemedium:') !!}
    {!! Form::number('priceMedium', null, ['class' => 'form-control']) !!}
</div>

<!-- Currency Field -->
<div class="form-group col-sm-6">
    {!! Form::label('currency', 'Currency:') !!}
    {!! Form::text('currency', null, ['class' => 'form-control','maxlength' => 191,'maxlength' => 191,'maxlength' => 191]) !!}
</div>

<!-- Priority Field -->
<div class="form-group col-sm-6">
    {!! Form::label('priority', 'Priority:') !!}
    {!! Form::number('priority', null, ['class' => 'form-control']) !!}
</div>

<!-- Isactive Field -->
<div class="form-group col-sm-6">
    {!! Form::label('isActive', 'Isactive:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('isActive', 0) !!}
        {!! Form::checkbox('isActive', '1', null) !!}
    </label>
</div>


<!-- Start Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('start_date', 'Start Date:') !!}
    {!! Form::text('start_date', null, ['class' => 'form-control','id'=>'start_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#start_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- End Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('end_date', 'End Date:') !!}
    {!! Form::text('end_date', null, ['class' => 'form-control','id'=>'end_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#end_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Stock Field -->
<div class="form-group col-sm-6">
    {!! Form::label('stock', 'Stock:') !!}
    {!! Form::number('stock', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', 'Image:') !!}
    {!! Form::text('image', null, ['class' => 'form-control','maxlength' => 191,'maxlength' => 191,'maxlength' => 191]) !!}
</div>

<!-- Category Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category_id', 'Category Id:') !!}
    {!! Form::number('category_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Createdby Field -->
<div class="form-group col-sm-6">
    {!! Form::label('createdBy', 'Createdby:') !!}
    {!! Form::number('createdBy', null, ['class' => 'form-control']) !!}
</div>

<!-- Updatedby Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updatedBy', 'Updatedby:') !!}
    {!! Form::number('updatedBy', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('products.index') }}" class="btn btn-default">Cancel</a>
</div>
