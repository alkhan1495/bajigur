<div class="table-responsive">
    <table class="table" id="products-table">
        <thead>
            <tr>
                <th>Sku</th>
        <th>Aliassku</th>
        <th>Productname</th>
        <th>Aliasname</th>
        <th>Description</th>
        <th>Uid</th>
        <th>Unitmeasurementsmall</th>
        <th>Unitmeasurementmedium</th>
        <th>Unitmeasurementbig</th>
        <th>Ukuran</th>
        <th>Nettweight</th>
        <th>Nettweightum</th>
        <th>Volume</th>
        <th>Smallinmediumamount</th>
        <th>Mediuminbigamount</th>
        <th>Pricebig</th>
        <th>Pricesmall</th>
        <th>Pricemedium</th>
        <th>Currency</th>
        <th>Priority</th>
        <th>Isactive</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Stock</th>
        <th>Image</th>
        <th>Category Id</th>
        <th>Createdby</th>
        <th>Updatedby</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
            <tr>
                <td>{{ $product->SKU }}</td>
            <td>{{ $product->aliasSKU }}</td>
            <td>{{ $product->productName }}</td>
            <td>{{ $product->aliasName }}</td>
            <td>{{ $product->description }}</td>
            <td>{{ $product->uid }}</td>
            <td>{{ $product->unitMeasurementSmall }}</td>
            <td>{{ $product->unitMeasurementMedium }}</td>
            <td>{{ $product->unitMeasurementBig }}</td>
            <td>{{ $product->ukuran }}</td>
            <td>{{ $product->nettWeight }}</td>
            <td>{{ $product->nettWeightUM }}</td>
            <td>{{ $product->volume }}</td>
            <td>{{ $product->smallInMediumAmount }}</td>
            <td>{{ $product->mediumInBigAmount }}</td>
            <td>{{ $product->priceBig }}</td>
            <td>{{ $product->priceSmall }}</td>
            <td>{{ $product->priceMedium }}</td>
            <td>{{ $product->currency }}</td>
            <td>{{ $product->priority }}</td>
            <td>{{ $product->isActive }}</td>
            <td>{{ $product->start_date }}</td>
            <td>{{ $product->end_date }}</td>
            <td>{{ $product->stock }}</td>
            <td>{{ $product->image }}</td>
            <td>{{ $product->category_id }}</td>
            <td>{{ $product->createdBy }}</td>
            <td>{{ $product->updatedBy }}</td>
                <td>
                    {!! Form::open(['route' => ['products.destroy', $product->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('products.show', [$product->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('products.edit', [$product->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
