<!-- Master Product Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('master_product_id', 'Master Product Id:') !!}
    {!! Form::number('master_product_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Agent Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('agent_id', 'Agent Id:') !!}
    {!! Form::number('agent_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Big Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price_big', 'Price Big:') !!}
    {!! Form::number('price_big', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Medium Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price_medium', 'Price Medium:') !!}
    {!! Form::number('price_medium', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Small Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price_small', 'Price Small:') !!}
    {!! Form::number('price_small', null, ['class' => 'form-control']) !!}
</div>

<!-- Alias Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('alias_name', 'Alias Name:') !!}
    {!! Form::text('alias_name', null, ['class' => 'form-control','maxlength' => 191,'maxlength' => 191,'maxlength' => 191]) !!}
</div>

<!-- Alias Sku Field -->
<div class="form-group col-sm-6">
    {!! Form::label('alias_sku', 'Alias Sku:') !!}
    {!! Form::text('alias_sku', null, ['class' => 'form-control','maxlength' => 191,'maxlength' => 191,'maxlength' => 191]) !!}
</div>

<!-- Is Active Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_active', 'Is Active:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_active', 0) !!}
        {!! Form::checkbox('is_active', '1', null) !!}
    </label>
</div>


<!-- Start Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('start_date', 'Start Date:') !!}
    {!! Form::text('start_date', null, ['class' => 'form-control','id'=>'start_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#start_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- End Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('end_date', 'End Date:') !!}
    {!! Form::text('end_date', null, ['class' => 'form-control','id'=>'end_date']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#end_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Pieceinbox Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pieceInBox', 'Pieceinbox:') !!}
    {!! Form::number('pieceInBox', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::number('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::number('updated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('productAgents.index') }}" class="btn btn-default">Cancel</a>
</div>
