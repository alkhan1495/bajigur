<!-- Master Product Id Field -->
<div class="form-group">
    {!! Form::label('master_product_id', 'Master Product Id:') !!}
    <p>{{ $productAgent->master_product_id }}</p>
</div>

<!-- Agent Id Field -->
<div class="form-group">
    {!! Form::label('agent_id', 'Agent Id:') !!}
    <p>{{ $productAgent->agent_id }}</p>
</div>

<!-- Price Big Field -->
<div class="form-group">
    {!! Form::label('price_big', 'Price Big:') !!}
    <p>{{ $productAgent->price_big }}</p>
</div>

<!-- Price Medium Field -->
<div class="form-group">
    {!! Form::label('price_medium', 'Price Medium:') !!}
    <p>{{ $productAgent->price_medium }}</p>
</div>

<!-- Price Small Field -->
<div class="form-group">
    {!! Form::label('price_small', 'Price Small:') !!}
    <p>{{ $productAgent->price_small }}</p>
</div>

<!-- Alias Name Field -->
<div class="form-group">
    {!! Form::label('alias_name', 'Alias Name:') !!}
    <p>{{ $productAgent->alias_name }}</p>
</div>

<!-- Alias Sku Field -->
<div class="form-group">
    {!! Form::label('alias_sku', 'Alias Sku:') !!}
    <p>{{ $productAgent->alias_sku }}</p>
</div>

<!-- Is Active Field -->
<div class="form-group">
    {!! Form::label('is_active', 'Is Active:') !!}
    <p>{{ $productAgent->is_active }}</p>
</div>

<!-- Start Date Field -->
<div class="form-group">
    {!! Form::label('start_date', 'Start Date:') !!}
    <p>{{ $productAgent->start_date }}</p>
</div>

<!-- End Date Field -->
<div class="form-group">
    {!! Form::label('end_date', 'End Date:') !!}
    <p>{{ $productAgent->end_date }}</p>
</div>

<!-- Pieceinbox Field -->
<div class="form-group">
    {!! Form::label('pieceInBox', 'Pieceinbox:') !!}
    <p>{{ $productAgent->pieceInBox }}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $productAgent->created_by }}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{{ $productAgent->updated_by }}</p>
</div>

