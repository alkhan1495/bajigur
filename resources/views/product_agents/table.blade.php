<div class="table-responsive">
    <table class="table" id="productAgents-table">
        <thead>
            <tr>
                <th>Master Product Id</th>
        <th>Agent Id</th>
        <th>Price Big</th>
        <th>Price Medium</th>
        <th>Price Small</th>
        <th>Alias Name</th>
        <th>Alias Sku</th>
        <th>Is Active</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Pieceinbox</th>
        <th>Created By</th>
        <th>Updated By</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($productAgents as $productAgent)
            <tr>
                <td>{{ $productAgent->master_product_id }}</td>
            <td>{{ $productAgent->agent_id }}</td>
            <td>{{ $productAgent->price_big }}</td>
            <td>{{ $productAgent->price_medium }}</td>
            <td>{{ $productAgent->price_small }}</td>
            <td>{{ $productAgent->alias_name }}</td>
            <td>{{ $productAgent->alias_sku }}</td>
            <td>{{ $productAgent->is_active }}</td>
            <td>{{ $productAgent->start_date }}</td>
            <td>{{ $productAgent->end_date }}</td>
            <td>{{ $productAgent->pieceInBox }}</td>
            <td>{{ $productAgent->created_by }}</td>
            <td>{{ $productAgent->updated_by }}</td>
                <td>
                    {!! Form::open(['route' => ['productAgents.destroy', $productAgent->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('productAgents.show', [$productAgent->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('productAgents.edit', [$productAgent->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
