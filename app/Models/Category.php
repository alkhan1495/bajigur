<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Category
 * @package App\Models
 * @version October 20, 2020, 10:31 am UTC
 *
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property boolean $is_active
 * @property string|\Carbon\Carbon $start_date
 * @property string|\Carbon\Carbon $end_date
 * @property string $image
 * @property integer $parent_id
 * @property integer $brand_id
 * @property integer $order
 */
class Category extends Model
{
    use SoftDeletes;

    public $table = 'categories';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'slug',
        'description',
        'is_active',
        'start_date',
        'end_date',
        'image',
        'parent_id',
        'brand_id',
        'order'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'slug' => 'string',
        'description' => 'string',
        'is_active' => 'boolean',
        'start_date' => 'datetime',
        'end_date' => 'datetime',
        'image' => 'string',
        'parent_id' => 'integer',
        'brand_id' => 'integer',
        'order' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string|max:191',
        'slug' => 'required|string|max:191',
        'description' => 'nullable|string',
        'is_active' => 'nullable|boolean',
        'start_date' => 'nullable',
        'end_date' => 'nullable',
        'image' => 'nullable|string|max:191',
        'parent_id' => 'nullable|integer',
        'brand_id' => 'nullable|integer',
        'order' => 'nullable|integer',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    
}
