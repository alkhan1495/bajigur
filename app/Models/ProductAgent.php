<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ProductAgent
 * @package App\Models
 * @version October 20, 2020, 10:31 am UTC
 *
 * @property integer $master_product_id
 * @property integer $agent_id
 * @property number $price_big
 * @property number $price_medium
 * @property number $price_small
 * @property string $alias_name
 * @property string $alias_sku
 * @property boolean $is_active
 * @property string $start_date
 * @property string $end_date
 * @property integer $pieceInBox
 * @property integer $created_by
 * @property integer $updated_by
 */
class ProductAgent extends Model
{
    use SoftDeletes;

    public $table = 'product_agent';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'master_product_id',
        'agent_id',
        'price_big',
        'price_medium',
        'price_small',
        'alias_name',
        'alias_sku',
        'is_active',
        'start_date',
        'end_date',
        'pieceInBox',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'master_product_id' => 'integer',
        'agent_id' => 'integer',
        'price_big' => 'decimal:2',
        'price_medium' => 'decimal:2',
        'price_small' => 'decimal:2',
        'alias_name' => 'string',
        'alias_sku' => 'string',
        'is_active' => 'boolean',
        'start_date' => 'date',
        'end_date' => 'date',
        'pieceInBox' => 'integer',
        'created_by' => 'integer',
        'updated_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'master_product_id' => 'nullable|integer',
        'agent_id' => 'nullable|integer',
        'price_big' => 'nullable|numeric',
        'price_medium' => 'nullable|numeric',
        'price_small' => 'nullable|numeric',
        'alias_name' => 'nullable|string|max:191',
        'alias_sku' => 'nullable|string|max:191',
        'is_active' => 'nullable|boolean',
        'start_date' => 'nullable',
        'end_date' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable',
        'pieceInBox' => 'nullable|integer',
        'created_by' => 'nullable|integer',
        'updated_by' => 'nullable|integer'
    ];

    
}
