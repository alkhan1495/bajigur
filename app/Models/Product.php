<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Product
 * @package App\Models
 * @version October 20, 2020, 10:31 am UTC
 *
 * @property \App\Models\User $createdby
 * @property string $SKU
 * @property string $aliasSKU
 * @property string $productName
 * @property string $aliasName
 * @property string $description
 * @property string $uid
 * @property string $unitMeasurementSmall
 * @property string $unitMeasurementMedium
 * @property string $unitMeasurementBig
 * @property string $ukuran
 * @property number $nettWeight
 * @property string $nettWeightUM
 * @property number $volume
 * @property integer $smallInMediumAmount
 * @property integer $mediumInBigAmount
 * @property number $priceBig
 * @property number $priceSmall
 * @property number $priceMedium
 * @property string $currency
 * @property integer $priority
 * @property boolean $isActive
 * @property string|\Carbon\Carbon $start_date
 * @property string|\Carbon\Carbon $end_date
 * @property number $stock
 * @property string $image
 * @property integer $category_id
 * @property integer $createdBy
 * @property integer $updatedBy
 */
class Product extends Model
{
    use SoftDeletes;

    public $table = 'products';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'SKU',
        'aliasSKU',
        'productName',
        'aliasName',
        'description',
        'uid',
        'unitMeasurementSmall',
        'unitMeasurementMedium',
        'unitMeasurementBig',
        'ukuran',
        'nettWeight',
        'nettWeightUM',
        'volume',
        'smallInMediumAmount',
        'mediumInBigAmount',
        'priceBig',
        'priceSmall',
        'priceMedium',
        'currency',
        'priority',
        'isActive',
        'start_date',
        'end_date',
        'stock',
        'image',
        'category_id',
        'createdBy',
        'updatedBy'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'SKU' => 'string',
        'aliasSKU' => 'string',
        'productName' => 'string',
        'aliasName' => 'string',
        'description' => 'string',
        'uid' => 'string',
        'unitMeasurementSmall' => 'string',
        'unitMeasurementMedium' => 'string',
        'unitMeasurementBig' => 'string',
        'ukuran' => 'string',
        'nettWeight' => 'float',
        'nettWeightUM' => 'string',
        'volume' => 'float',
        'smallInMediumAmount' => 'integer',
        'mediumInBigAmount' => 'integer',
        'priceBig' => 'float',
        'priceSmall' => 'float',
        'priceMedium' => 'decimal:2',
        'currency' => 'string',
        'priority' => 'integer',
        'isActive' => 'boolean',
        'start_date' => 'datetime',
        'end_date' => 'datetime',
        'stock' => 'float',
        'image' => 'string',
        'category_id' => 'integer',
        'createdBy' => 'integer',
        'updatedBy' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'SKU' => 'nullable|string|max:191',
        'aliasSKU' => 'nullable|string|max:191',
        'productName' => 'nullable|string|max:191',
        'aliasName' => 'nullable|string|max:191',
        'description' => 'nullable|string|max:191',
        'uid' => 'nullable|string|max:191',
        'unitMeasurementSmall' => 'nullable|string|max:191',
        'unitMeasurementMedium' => 'nullable|string|max:191',
        'unitMeasurementBig' => 'nullable|string|max:191',
        'ukuran' => 'nullable|string|max:191',
        'nettWeight' => 'nullable|numeric',
        'nettWeightUM' => 'nullable|string|max:191',
        'volume' => 'nullable|numeric',
        'smallInMediumAmount' => 'required|integer',
        'mediumInBigAmount' => 'nullable|integer',
        'priceBig' => 'nullable|numeric',
        'priceSmall' => 'nullable|numeric',
        'priceMedium' => 'nullable|numeric',
        'currency' => 'nullable|string|max:191',
        'priority' => 'nullable|integer',
        'isActive' => 'nullable|boolean',
        'start_date' => 'nullable',
        'end_date' => 'nullable',
        'stock' => 'nullable|numeric',
        'image' => 'nullable|string|max:191',
        'category_id' => 'nullable|integer',
        'created_at' => 'nullable',
        'createdBy' => 'nullable|integer',
        'updatedBy' => 'nullable|integer',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function createdby()
    {
        return $this->belongsTo(\App\Models\User::class, 'createdBy');
    }
}
