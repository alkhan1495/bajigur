<?php

namespace App\Repositories;

use App\Models\ProductAgent;
use App\Repositories\BaseRepository;

/**
 * Class ProductAgentRepository
 * @package App\Repositories
 * @version October 20, 2020, 10:31 am UTC
*/

class ProductAgentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'master_product_id',
        'agent_id',
        'price_big',
        'price_medium',
        'price_small',
        'alias_name',
        'alias_sku',
        'is_active',
        'start_date',
        'end_date',
        'pieceInBox',
        'created_by',
        'updated_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProductAgent::class;
    }
}
