<?php

namespace App\Repositories;

use App\Models\Product;
use App\Repositories\BaseRepository;

/**
 * Class ProductRepository
 * @package App\Repositories
 * @version October 20, 2020, 10:31 am UTC
*/

class ProductRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'SKU',
        'aliasSKU',
        'productName',
        'aliasName',
        'description',
        'uid',
        'unitMeasurementSmall',
        'unitMeasurementMedium',
        'unitMeasurementBig',
        'ukuran',
        'nettWeight',
        'nettWeightUM',
        'volume',
        'smallInMediumAmount',
        'mediumInBigAmount',
        'priceBig',
        'priceSmall',
        'priceMedium',
        'currency',
        'priority',
        'isActive',
        'start_date',
        'end_date',
        'stock',
        'image',
        'category_id',
        'createdBy',
        'updatedBy'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Product::class;
    }
}
