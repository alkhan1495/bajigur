<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductAgentRequest;
use App\Http\Requests\UpdateProductAgentRequest;
use App\Repositories\ProductAgentRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ProductAgentController extends AppBaseController
{
    /** @var  ProductAgentRepository */
    private $productAgentRepository;

    public function __construct(ProductAgentRepository $productAgentRepo)
    {
        $this->productAgentRepository = $productAgentRepo;
    }

    /**
     * Display a listing of the ProductAgent.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $productAgents = $this->productAgentRepository->all();

        return view('product_agents.index')
            ->with('productAgents', $productAgents);
    }

    /**
     * Show the form for creating a new ProductAgent.
     *
     * @return Response
     */
    public function create()
    {
        return view('product_agents.create');
    }

    /**
     * Store a newly created ProductAgent in storage.
     *
     * @param CreateProductAgentRequest $request
     *
     * @return Response
     */
    public function store(CreateProductAgentRequest $request)
    {
        $input = $request->all();

        $productAgent = $this->productAgentRepository->create($input);

        Flash::success('Product Agent saved successfully.');

        return redirect(route('productAgents.index'));
    }

    /**
     * Display the specified ProductAgent.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $productAgent = $this->productAgentRepository->find($id);

        if (empty($productAgent)) {
            Flash::error('Product Agent not found');

            return redirect(route('productAgents.index'));
        }

        return view('product_agents.show')->with('productAgent', $productAgent);
    }

    /**
     * Show the form for editing the specified ProductAgent.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $productAgent = $this->productAgentRepository->find($id);

        if (empty($productAgent)) {
            Flash::error('Product Agent not found');

            return redirect(route('productAgents.index'));
        }

        return view('product_agents.edit')->with('productAgent', $productAgent);
    }

    /**
     * Update the specified ProductAgent in storage.
     *
     * @param int $id
     * @param UpdateProductAgentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductAgentRequest $request)
    {
        $productAgent = $this->productAgentRepository->find($id);

        if (empty($productAgent)) {
            Flash::error('Product Agent not found');

            return redirect(route('productAgents.index'));
        }

        $productAgent = $this->productAgentRepository->update($request->all(), $id);

        Flash::success('Product Agent updated successfully.');

        return redirect(route('productAgents.index'));
    }

    /**
     * Remove the specified ProductAgent from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $productAgent = $this->productAgentRepository->find($id);

        if (empty($productAgent)) {
            Flash::error('Product Agent not found');

            return redirect(route('productAgents.index'));
        }

        $this->productAgentRepository->delete($id);

        Flash::success('Product Agent deleted successfully.');

        return redirect(route('productAgents.index'));
    }
}
