<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateProductAgentAPIRequest;
use App\Http\Requests\API\UpdateProductAgentAPIRequest;
use App\Models\ProductAgent;
use App\Repositories\ProductAgentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ProductAgentController
 * @package App\Http\Controllers\API
 */

class ProductAgentAPIController extends AppBaseController
{
    /** @var  ProductAgentRepository */
    private $productAgentRepository;

    public function __construct(ProductAgentRepository $productAgentRepo)
    {
        $this->productAgentRepository = $productAgentRepo;
    }

    /**
     * Display a listing of the ProductAgent.
     * GET|HEAD /productAgents
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $productAgents = $this->productAgentRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($productAgents->toArray(), 'Product Agents retrieved successfully');
    }

    /**
     * Store a newly created ProductAgent in storage.
     * POST /productAgents
     *
     * @param CreateProductAgentAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateProductAgentAPIRequest $request)
    {
        $input = $request->all();

        $productAgent = $this->productAgentRepository->create($input);

        return $this->sendResponse($productAgent->toArray(), 'Product Agent saved successfully');
    }

    /**
     * Display the specified ProductAgent.
     * GET|HEAD /productAgents/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ProductAgent $productAgent */
        $productAgent = $this->productAgentRepository->find($id);

        if (empty($productAgent)) {
            return $this->sendError('Product Agent not found');
        }

        return $this->sendResponse($productAgent->toArray(), 'Product Agent retrieved successfully');
    }

    /**
     * Update the specified ProductAgent in storage.
     * PUT/PATCH /productAgents/{id}
     *
     * @param int $id
     * @param UpdateProductAgentAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductAgentAPIRequest $request)
    {
        $input = $request->all();

        /** @var ProductAgent $productAgent */
        $productAgent = $this->productAgentRepository->find($id);

        if (empty($productAgent)) {
            return $this->sendError('Product Agent not found');
        }

        $productAgent = $this->productAgentRepository->update($input, $id);

        return $this->sendResponse($productAgent->toArray(), 'ProductAgent updated successfully');
    }

    /**
     * Remove the specified ProductAgent from storage.
     * DELETE /productAgents/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ProductAgent $productAgent */
        $productAgent = $this->productAgentRepository->find($id);

        if (empty($productAgent)) {
            return $this->sendError('Product Agent not found');
        }

        $productAgent->delete();

        return $this->sendSuccess('Product Agent deleted successfully');
    }
}
