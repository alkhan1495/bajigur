/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : 127.0.0.1:3306
 Source Schema         : bajigur

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 20/10/2020 17:44:01
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `is_active` tinyint(1) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int unsigned DEFAULT NULL,
  `brand_id` int unsigned DEFAULT NULL,
  `order` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `categories_brand_id_foreign` (`brand_id`) USING BTREE,
  CONSTRAINT `categories_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=676 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of categories
-- ----------------------------
BEGIN;
INSERT INTO `categories` VALUES (1, 'Rucika Fitting JIS', 'rucika-fitting-jis', 'Rucika|Rucika Fitting JIS', 1, '2018-01-01 00:00:00', NULL, 'uploads/category_images/ZVnMQwswgGSQCvTddu0K.jpg', NULL, 1, 3, NULL, '2020-10-13 22:06:39', NULL);
INSERT INTO `categories` VALUES (2, 'Rucika Kelen Green', 'rucika-kelen-green', 'Rucika|Rucika Kelen Green', 1, '2018-01-01 00:00:00', NULL, 'uploads/category_images/0uZmCc0bdI9rQSqKkFWt.png', NULL, 1, 4, NULL, '2020-10-13 22:10:21', NULL);
INSERT INTO `categories` VALUES (3, 'Rucika JIS', 'rucika-jis', 'Rucika|Rucika JIS', 1, '2018-01-01 00:00:00', NULL, 'uploads/category_images/MQh8skX0iKHD8tnov6Pw.jpg', NULL, 1, 2, NULL, '2019-07-17 12:11:05', NULL);
INSERT INTO `categories` VALUES (4, 'Rucika Standard', 'rucika-standard', 'Rucika|Rucika Standard', 1, '2018-01-01 00:00:00', NULL, 'uploads/category_images/WQHLAFIVQ9HLT2loh0T2.png', NULL, 1, 1, NULL, '2019-07-01 22:35:49', NULL);
INSERT INTO `categories` VALUES (5, 'Rucika Wavin', 'rucika-wavin', 'Rucika|Rucika Wavin', 0, '2018-01-01 00:00:00', NULL, NULL, NULL, 1, 6, NULL, '2019-11-20 15:59:25', NULL);
INSERT INTO `categories` VALUES (6, 'RuGlue', 'ruglue', 'Rucika|RuGlue', 1, '2018-01-01 00:00:00', NULL, 'uploads/category_images/LPBIrKfP6SMGUSjaHjqi.png', NULL, 1, 5, NULL, '2019-07-17 14:22:14', NULL);
INSERT INTO `categories` VALUES (7, 'Atap Djabesmen', 'atap-djabesmen', 'Djabesmen|Atap Djabesmen', 1, '2018-01-01 00:00:00', NULL, 'uploads/category_images/5OD36wRR2RcqHplkrm9O.jpg', NULL, 2, 1, NULL, '2018-11-23 21:18:31', NULL);
INSERT INTO `categories` VALUES (8, 'Ambience', 'ambience', 'Granito|Ambience', 1, '2018-01-01 00:00:00', NULL, 'uploads/category_images/WLssF9brUvBgNnjYzNwW.jpg', NULL, 3, 1, NULL, '2018-11-23 18:39:02', NULL);
INSERT INTO `categories` VALUES (9, 'Aurora', 'aurora', 'Granito|Aurora', 1, '2018-01-01 00:00:00', NULL, 'uploads/category_images/46gD19bXDRwsjh1xNjUP.jpg', NULL, 3, 2, NULL, '2018-11-23 18:39:16', NULL);
INSERT INTO `categories` VALUES (10, 'Castello', 'castello', 'Granito|Castello', 1, '2018-01-01 00:00:00', NULL, 'uploads/category_images/kegtYUbO1NQOdcKaxPCR.jpg', NULL, 3, 3, NULL, '2018-11-23 18:39:32', NULL);
COMMIT;

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
BEGIN;
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_08_19_000000_create_failed_jobs_table', 1);
COMMIT;

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for product_agent
-- ----------------------------
DROP TABLE IF EXISTS `product_agent`;
CREATE TABLE `product_agent` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `master_product_id` int DEFAULT NULL,
  `agent_id` int DEFAULT NULL,
  `price_big` decimal(15,2) DEFAULT NULL,
  `price_medium` decimal(15,2) DEFAULT NULL,
  `price_small` decimal(15,2) DEFAULT NULL,
  `alias_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alias_sku` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `pieceInBox` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `product_agent_id_index` (`master_product_id`) USING BTREE,
  KEY `index_product_agents` (`agent_id`,`alias_sku`,`master_product_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=441792 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of product_agent
-- ----------------------------
BEGIN;
INSERT INTO `product_agent` VALUES (1, 1, 1, NULL, NULL, NULL, 'RUCIKA FITTING KELEN GREEN EQUAL TEE DIA 25 MM', '21064109025000', 1, NULL, NULL, '2018-10-25 22:37:59', '2020-09-09 13:32:19', NULL, NULL, NULL, NULL);
INSERT INTO `product_agent` VALUES (2, 2, 1, NULL, NULL, NULL, 'RUCIKA FITTING KELEN GREEN EQUAL TEE DIA 110 MM', '21064109110000', 1, NULL, NULL, '2018-10-25 22:37:59', '2020-09-09 13:32:19', NULL, NULL, NULL, NULL);
INSERT INTO `product_agent` VALUES (3, 3, 1, NULL, NULL, NULL, 'RUCIKA FITTING KELEN GREEN EQUAL TEE DIA 125 MM', '21064109125000', 1, NULL, NULL, '2018-10-25 22:37:59', '2020-09-09 13:32:19', NULL, NULL, NULL, NULL);
INSERT INTO `product_agent` VALUES (4, 4, 1, NULL, NULL, NULL, 'RUCIKA FITTING KELEN GREEN EQUAL TEE DIA 160 MM', '21064109160000', 1, NULL, NULL, '2018-10-25 22:37:59', '2020-09-09 13:32:19', NULL, NULL, NULL, NULL);
INSERT INTO `product_agent` VALUES (5, 5, 1, NULL, NULL, NULL, 'RUCIKA FITTING KELEN GREEN FEMALE THREAD JOINT 20x1/2', '10064114020001', 1, NULL, NULL, '2018-10-25 22:37:59', '2020-09-09 13:32:19', NULL, NULL, NULL, NULL);
INSERT INTO `product_agent` VALUES (6, 6, 1, NULL, NULL, NULL, 'Rucika Green Female Thread Joint 20 x 1/2 in', '20064114020001', 1, NULL, NULL, '2018-10-25 22:37:59', '2020-09-09 13:32:19', NULL, NULL, NULL, NULL);
INSERT INTO `product_agent` VALUES (7, 7, 1, NULL, NULL, NULL, 'RUCIKA FITTING KELEN GREEN FEMALE THREAD JOINT 20x3/4', '10064114020002', 1, NULL, NULL, '2018-10-25 22:37:59', '2020-09-09 13:32:19', NULL, NULL, NULL, NULL);
INSERT INTO `product_agent` VALUES (8, 8, 1, NULL, NULL, NULL, 'Rucika Fitting JIS Socket AW 3/4', '20014101026000', 1, NULL, NULL, '2018-10-25 22:37:59', '2020-09-09 13:32:19', NULL, NULL, NULL, NULL);
INSERT INTO `product_agent` VALUES (9, 9, 1, NULL, NULL, NULL, 'RUCIKA FITTING KELEN GREEN FEMALE THREAD JOINT 25x1/2', '10064114025001', 1, NULL, NULL, '2018-10-25 22:37:59', '2020-09-09 13:32:19', NULL, NULL, NULL, NULL);
INSERT INTO `product_agent` VALUES (10, 10, 1, NULL, NULL, NULL, 'Rucika Fitting JIS 45 Elbow AW 1 1/2', '20014114048000', 1, NULL, NULL, '2018-10-25 22:38:00', '2020-09-09 13:32:19', NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `SKU` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aliasSKU` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `productName` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aliasName` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uid` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unitMeasurementSmall` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unitMeasurementMedium` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unitMeasurementBig` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ukuran` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nettWeight` double DEFAULT NULL,
  `nettWeightUM` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `volume` double DEFAULT NULL,
  `smallInMediumAmount` int NOT NULL DEFAULT '0',
  `mediumInBigAmount` int DEFAULT NULL,
  `priceBig` double DEFAULT NULL,
  `priceSmall` double DEFAULT NULL,
  `priceMedium` decimal(15,2) DEFAULT NULL,
  `currency` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority` int DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `stock` double DEFAULT NULL,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `createdBy` int unsigned DEFAULT NULL,
  `updatedBy` int DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `products_createdby_foreign` (`createdBy`) USING BTREE,
  KEY `products_category_id_index` (`category_id`) USING BTREE,
  CONSTRAINT `products_createdby_foreign` FOREIGN KEY (`createdBy`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=3124 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of products
-- ----------------------------
BEGIN;
INSERT INTO `products` VALUES (1, '21064109025000', '21064109025000', 'EQUAL TEE DIA 25 MM', 'Rucika Green Equal Tee 3/4 in. 25 mm', '', '', 'pcs', 'pcs', 'pcs', '', 0.038, 'kg', 0, 1, 1, 0, 8690, 0.00, 'IDR', 0, 1, '2019-09-16 00:00:00', '2098-09-01 00:00:00', NULL, 'uploads/product_image/F8253gs0yUcZzpbZwjnL.jpg', 75, '2018-10-25 17:30:04', 1, 149, '2020-08-01 15:48:17', NULL);
INSERT INTO `products` VALUES (2, '21064109110000', '21064109110000', 'EQUAL TEE DIA 110 MM', 'EQUAL TEE DIA 110 MM', '', '', 'pcs', 'pcs', 'pcs', '', 1.863, 'kg', 0, 1, 1, 0, 389400, 0.00, 'IDR', 0, 1, '2019-09-16 00:00:00', '2098-09-01 00:00:00', NULL, NULL, 75, '2018-10-25 17:30:04', 1, 149, '2020-08-01 15:48:17', NULL);
INSERT INTO `products` VALUES (3, '21064109125000', '21064109125000', 'EQUAL TEE DIA 125 MM', 'Rucika Green Equal Tee 5 in. 125 mm', '', '', 'pcs', 'pcs', 'pcs', '', 0, 'kg', 0, 1, 1, 0, 1546270, 0.00, 'IDR', 0, 1, '2019-09-16 00:00:00', '2098-09-01 00:00:00', NULL, NULL, 75, '2018-10-25 17:30:04', 1, 149, '2020-08-01 15:48:17', NULL);
INSERT INTO `products` VALUES (4, '21064109160000', '21064109160000', 'EQUAL TEE DIA 160 MM', 'Rucika Green Equal Tee 6 in. 160 mm', '', '', 'pcs', 'pcs', 'pcs', '', 0, 'kg', 0, 1, 1, 0, 1984510, 0.00, 'IDR', 0, 1, '2019-09-16 00:00:00', '2098-09-01 00:00:00', NULL, NULL, 75, '2018-10-25 17:30:04', 1, 149, '2020-08-01 15:48:17', NULL);
INSERT INTO `products` VALUES (5, '10064114020001', '10064114020001', 'FEMALE THREAD JOINT 20x1/2 in.', 'Rucika Green Female Thread Joint 20 x 1/2 in', '', '', 'pcs', 'pcs', 'pcs', '', 0.059, 'kg', 0, 1, 1, 0, 31350, 0.00, 'IDR', 0, 1, '2019-09-17 00:00:00', '2098-09-01 00:00:00', NULL, NULL, 76, '2018-10-25 17:30:04', 1, 149, '2020-08-10 00:33:35', NULL);
INSERT INTO `products` VALUES (6, '20064114020001', '20064114020001', 'Rucika Green Female Thread Joint 20 x 1/2 in', 'Rucika Green Female Thread Joint 20 x 1/2 in', '', '', 'pcs', 'pcs', 'pcs', '', 0.05, 'kg', 0, 1, 1, 0, 31350, 0.00, 'IDR', 0, 1, '2019-01-01 00:00:00', '2088-11-01 00:00:00', NULL, NULL, 76, '2018-10-25 17:30:04', 1, NULL, '2019-07-24 16:07:34', NULL);
INSERT INTO `products` VALUES (7, '10064114020002', '10064114020002', 'FEMALE THREAD JOINT 20x3/4 in.', 'Rucika Green Female Thread Joint 20 x 3/4 in', '', '', 'pcs', 'pcs', 'pcs', '', 0.075, 'kg', 0, 1, 1, 0, 45870, 0.00, 'IDR', 0, 1, '2019-09-17 00:00:00', '2098-09-01 00:00:00', NULL, NULL, 76, '2018-10-25 17:30:04', 1, 149, '2020-08-10 00:33:35', NULL);
INSERT INTO `products` VALUES (8, '20014101026000', '20014101026000', 'RUCIKA FITTING JIS SOCKET AW 3/4', 'RUCIKA FITTING JIS SOCKET AW 3/4', '', '', 'pcs', 'box', 'box', '', 0.0395, 'kg', 0, 200, 1, 0, 2000, 400000.00, 'IDR', 0, 1, '2019-09-16 00:00:00', '2098-09-01 00:00:00', NULL, NULL, 58, '2018-10-25 17:30:04', 1, 149, '2020-08-01 15:48:17', NULL);
INSERT INTO `products` VALUES (9, '10064114025001', '10064114025001', 'FEMALE THREAD JOINT 25x1/2 in.', 'Rucika Green Female Thread Joint 25 x 1/2 in', '', '', 'pcs', 'pcs', 'pcs', '', 0.066, 'kg', 0, 1, 1, 0, 33880, 0.00, 'IDR', 0, 1, '2019-09-17 00:00:00', '2098-09-01 00:00:00', NULL, NULL, 76, '2018-10-25 17:30:04', 1, 149, '2020-08-10 00:33:35', NULL);
INSERT INTO `products` VALUES (10, '20014114048000', '20014114048000', 'RUCIKA FITTING JIS 45 DGR ELBOW AW-45L 1 1/2', 'RUCIKA FITTING JIS 45 DGR ELBOW AW-45L 1 1/2', '', '', 'pcs', 'box', 'box', '', 0.149444444, 'kg', 0, 27, 1, 259200, 9600, 259200.00, 'IDR', 0, 1, '2019-09-16 00:00:00', '2098-09-01 00:00:00', NULL, NULL, 30, '2018-10-25 17:30:04', 1, NULL, '2020-08-01 15:48:17', NULL);
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES (1, 'test', 'alkhannaksibuk@gmail.com', NULL, '$2y$10$C8w24En/mWAQbPWKpUoZT.msS.YOVVsNS7qK7eUIFZ5sYpjN60ONa', NULL, '2020-10-20 10:29:15', '2020-10-20 10:29:15');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
