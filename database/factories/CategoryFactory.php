<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Category::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
        'slug' => $this->faker->word,
        'description' => $this->faker->text,
        'is_active' => $this->faker->word,
        'start_date' => $this->faker->date('Y-m-d H:i:s'),
        'end_date' => $this->faker->date('Y-m-d H:i:s'),
        'image' => $this->faker->word,
        'parent_id' => $this->faker->randomDigitNotNull,
        'brand_id' => $this->faker->randomDigitNotNull,
        'order' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
