<?php

namespace Database\Factories;

use App\Models\ProductAgent;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductAgentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProductAgent::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'master_product_id' => $this->faker->randomDigitNotNull,
        'agent_id' => $this->faker->randomDigitNotNull,
        'price_big' => $this->faker->word,
        'price_medium' => $this->faker->word,
        'price_small' => $this->faker->word,
        'alias_name' => $this->faker->word,
        'alias_sku' => $this->faker->word,
        'is_active' => $this->faker->word,
        'start_date' => $this->faker->word,
        'end_date' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s'),
        'pieceInBox' => $this->faker->randomDigitNotNull,
        'created_by' => $this->faker->randomDigitNotNull,
        'updated_by' => $this->faker->randomDigitNotNull
        ];
    }
}
