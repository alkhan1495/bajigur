<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'SKU' => $this->faker->word,
        'aliasSKU' => $this->faker->word,
        'productName' => $this->faker->word,
        'aliasName' => $this->faker->word,
        'description' => $this->faker->word,
        'uid' => $this->faker->word,
        'unitMeasurementSmall' => $this->faker->word,
        'unitMeasurementMedium' => $this->faker->word,
        'unitMeasurementBig' => $this->faker->word,
        'ukuran' => $this->faker->word,
        'nettWeight' => $this->faker->randomDigitNotNull,
        'nettWeightUM' => $this->faker->word,
        'volume' => $this->faker->randomDigitNotNull,
        'smallInMediumAmount' => $this->faker->randomDigitNotNull,
        'mediumInBigAmount' => $this->faker->randomDigitNotNull,
        'priceBig' => $this->faker->randomDigitNotNull,
        'priceSmall' => $this->faker->randomDigitNotNull,
        'priceMedium' => $this->faker->word,
        'currency' => $this->faker->word,
        'priority' => $this->faker->randomDigitNotNull,
        'isActive' => $this->faker->word,
        'start_date' => $this->faker->date('Y-m-d H:i:s'),
        'end_date' => $this->faker->date('Y-m-d H:i:s'),
        'stock' => $this->faker->randomDigitNotNull,
        'image' => $this->faker->word,
        'category_id' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'createdBy' => $this->faker->randomDigitNotNull,
        'updatedBy' => $this->faker->randomDigitNotNull,
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
